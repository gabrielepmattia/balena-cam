#!/bin/bash
DEBIAN_FRONTEND=noninteractive
HOME=/root

PYTHON_V=3.11.1
PYTHON3=$HOME/.asdf/installs/python/$PYTHON_V/bin/python3
PIP3=$HOME/.asdf/installs/python/$PYTHON_V/bin/pip3
ASDF=$HOME/.asdf/bin/asdf