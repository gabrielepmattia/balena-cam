import asyncio, json, os, cv2, platform, sys
from time import sleep
from aiohttp import web
from av import VideoFrame
from aiortc import (
    RTCPeerConnection,
    RTCSessionDescription,
    VideoStreamTrack,
    RTCIceServer,
    RTCConfiguration,
)

# from aiohttp_basicauth import BasicAuthMiddleware

ENV_CAMERA_DRIVER = "CAM_CAMERA_DRIVER"  # or nvargus
ENV_CAMERA_DEVICE_ID = "CAM_CAMERA_ID"
ENV_CAMERA_DRIVER_VALUE_V4L2 = "v4l2"
ENV_CAMERA_DRIVER_VALUE_NVARGUS = "nvargus"


class CameraDevice:
    def __init__(self):
        self.cap = None

        while self.cap == None:
            self.cap = self.get_video_capture()
            sleep(5)

        print("[CameraDevice] ready!")

        # ret, frame = self.cap.read()
        # if not ret:
        #     print(f'Failed to open default camera ({ret}). Exiting...')
        #     sys.exit()
        # self.cap.set(3, 640)
        # self.cap.set(4, 480)

    def get_video_capture(self):
        try:
            # cap = cv2.VideoCapture(1)
            # cap = cv2.VideoCapture(CameraDevice.gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
            # cap = cv2.VideoCapture('nvarguscamerasrc ! video/x-raw(memory:NVMM), width=640, height=480, format=(string)NV12, framerate=(fraction)20/1 ! nvvidconv ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink' , cv2.CAP_GSTREAMER)
            # cap = cv2.VideoCapture('nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)NV12, framerate=(fraction)10/1 ! nvvidconv flip-method=2 ! video/x-raw, width=(int)1280, height=(int)720, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink', cv2.CAP_GSTREAMER)
            # cap = cv2.VideoCapture(
            #     "nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM), width=(int)1920, height=(int)1080, format=(string)NV12, framerate=(fraction)30/1 ! nvvidconv flip-method=2 ! video/x-raw, width=(int)1920, height=(int)1080, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink",
            #     cv2.CAP_GSTREAMER,
            # )

            # pipeline = CameraDevice.pipeline_gstreamer(
            #     sensor_id=0,
            #     capture_width=3264,  # 3264,
            #     capture_height=1848,  # 2464,
            #     display_width=3264,  # 3264,
            #     display_height=1848,  # 2464,
            #     framerate=28,
            # )

            # pipeline = CameraDevice.pipeline_v4l2(
            #     sensor_path="/dev/video1",
            #     capture_width=1920,  # 3264,
            #     capture_height=1080,  # 2464,
            #     display_width=1920,  # 3264,
            #     display_height=1080,  # 2464,
            #     framerate=30,
            # )

            # print(f"[CameraDevice] using pipeline={pipeline}")

            # cap = cv2.VideoCapture(
            #     pipeline,
            #     cv2.CAP_GSTREAMER,
            # )

            cap = None

            if ENV_CAMERA_DRIVER in os.environ:
                if ENV_CAMERA_DRIVER == ENV_CAMERA_DRIVER_VALUE_V4L2:
                    cap = cv2.VideoCapture(int(ENV_CAMERA_DEVICE_ID))
                elif ENV_CAMERA_DRIVER == ENV_CAMERA_DRIVER_VALUE_NVARGUS:
                    pipeline = CameraDevice.pipeline_gstreamer(
                        sensor_id=int(ENV_CAMERA_DEVICE_ID),
                        capture_width=1920,  # 3264,
                        capture_height=1080,  # 2464,
                        display_width=1920,  # 3264,
                        display_height=1080,  # 2464,
                        framerate=30,
                    )
                    cap = cv2.VideoCapture(
                        pipeline,
                        cv2.CAP_GSTREAMER,
                    )
                else:
                     cap = cv2.VideoCapture(0)
            else:
                cap = cv2.VideoCapture(0)

            if not cap.isOpened():
                print(f"Failed to open default camera")
                return None

            ret, frame = cap.read()
            if not ret:
                print(f"Failed to get frame from camera ({ret})")
                return None
            return cap
        except Exception as e:
            print(f"Failed to open default camera ({e})")
            return None

    def rotate(self, frame):
        if flip:
            (h, w) = frame.shape[:2]
            center = (w / 2, h / 2)
            M = cv2.getRotationMatrix2D(center, 180, 1.0)
            frame = cv2.warpAffine(frame, M, (w, h))
        return frame

    async def get_latest_frame(self):
        # print(f"[CameraDevice] get_latest_frame: called, self.cap={self.cap}")
        ret, frame = self.cap.read()
        # await asyncio.sleep(0)
        return self.rotate(frame)

    async def get_jpeg_frame(self):
        encode_param = (int(cv2.IMWRITE_JPEG_QUALITY), 100)
        frame = await self.get_latest_frame()
        frame, encimg = cv2.imencode(".jpg", frame, encode_param)
        return encimg.tostring()

    @staticmethod
    def pipeline_v4l2(
        sensor_path="/dev/video0",
        capture_width=1280,
        capture_height=720,
        display_width=1280,
        display_height=720,
        framerate=30,
        flip_method=2,
    ):
        return (
            f"v4l2src device={sensor_path}"
            f" ! video/x-raw, framerate={framerate}/1, width={capture_width}, height={capture_height}"
            f" ! videoconvert"
            f" ! appsink"
        )

    @staticmethod
    def pipeline_gstreamer(
        sensor_id=0,
        capture_width=1280,
        capture_height=720,
        display_width=1280,
        display_height=720,
        framerate=30,
        flip_method=2,
    ):
        return (
            f"nvarguscamerasrc sensor-id={sensor_id}"
            f" ! video/x-raw(memory:NVMM), width=(int){capture_width}, height=(int){capture_height}, format=(string)NV12, framerate=(fraction){framerate}/1"
            f" ! nvvidconv flip-method={flip_method}"
            f" ! video/x-raw, width=(int){display_width}, height=(int){display_height}, format=(string)BGRx"
            f" ! videoconvert ! video/x-raw, format=(string)BGR"
            f" ! appsink"
        )


class PeerConnectionFactory:
    def __init__(self):
        self.config = {"sdpSemantics": "unified-plan"}
        self.STUN_SERVER = None
        self.TURN_SERVER = None
        self.TURN_USERNAME = None
        self.TURN_PASSWORD = None
        if all(
            k in os.environ
            for k in ("STUN_SERVER", "TURN_SERVER", "TURN_USERNAME", "TURN_PASSWORD")
        ):
            print("WebRTC connections will use your custom ICE Servers (STUN / TURN).")
            self.STUN_SERVER = os.environ["STUN_SERVER"]
            self.TURN_SERVER = os.environ["TURN_SERVER"]
            self.TURN_USERNAME = os.environ["TURN_USERNAME"]
            self.TURN_PASSWORD = os.environ["TURN_PASSWORD"]
            iceServers = [
                {"urls": self.STUN_SERVER},
                {
                    "urls": self.TURN_SERVER,
                    "credential": self.TURN_PASSWORD,
                    "username": self.TURN_USERNAME,
                },
            ]
            self.config["iceServers"] = iceServers

    def create_peer_connection(self):
        if self.TURN_SERVER is not None:
            iceServers = []
            iceServers.append(RTCIceServer(self.STUN_SERVER))
            iceServers.append(
                RTCIceServer(
                    self.TURN_SERVER,
                    username=self.TURN_USERNAME,
                    credential=self.TURN_PASSWORD,
                )
            )
            return RTCPeerConnection(RTCConfiguration(iceServers))
        return RTCPeerConnection()

    def get_ice_config(self):
        return json.dumps(self.config)


class RTCVideoStream(VideoStreamTrack):
    def __init__(self, camera_device):
        super().__init__()
        self.camera_device = camera_device
        self.data_bgr = None

    async def recv(self):
        self.data_bgr = await self.camera_device.get_latest_frame()
        frame = VideoFrame.from_ndarray(self.data_bgr, format="bgr24")
        pts, time_base = await self.next_timestamp()
        frame.pts = pts
        frame.time_base = time_base
        return frame


async def index(request):
    content = open(os.path.join(ROOT, "client/index.html"), "r").read()
    return web.Response(content_type="text/html", text=content)


async def stylesheet(request):
    content = open(os.path.join(ROOT, "client/style.css"), "r").read()
    return web.Response(content_type="text/css", text=content)


async def javascript(request):
    content = open(os.path.join(ROOT, "client/client.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)


async def balena(request):
    content = open(os.path.join(ROOT, "client/balena-cam.svg"), "r").read()
    return web.Response(content_type="image/svg+xml", text=content)


async def balena_logo(request):
    content = open(os.path.join(ROOT, "client/balena-logo.svg"), "r").read()
    return web.Response(content_type="image/svg+xml", text=content)


async def favicon(request):
    return web.FileResponse(os.path.join(ROOT, "client/favicon.png"))


async def offer(request):
    params = await request.json()
    offer = RTCSessionDescription(sdp=params["sdp"], type=params["type"])
    pc = pc_factory.create_peer_connection()
    pcs.add(pc)
    # Add local media
    local_video = RTCVideoStream(camera_device)
    pc.addTrack(local_video)

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        if pc.iceConnectionState == "failed":
            await pc.close()
            pcs.discard(pc)

    await pc.setRemoteDescription(offer)
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)
    return web.Response(
        content_type="application/json",
        text=json.dumps(
            {"sdp": pc.localDescription.sdp, "type": pc.localDescription.type}
        ),
    )


async def mjpeg_handler(request):
    boundary = "frame"
    response = web.StreamResponse(
        status=200,
        reason="OK",
        headers={
            "Content-Type": "multipart/x-mixed-replace; " "boundary=%s" % boundary,
        },
    )
    await response.prepare(request)
    while True:
        data = await camera_device.get_jpeg_frame()
        await asyncio.sleep(1 / 30)  # this means that the maximum FPS is 30
        await response.write("--{}\r\n".format(boundary).encode("utf-8"))
        await response.write(b"Content-Type: image/jpeg\r\n")
        await response.write("Content-Length: {}\r\n".format(len(data)).encode("utf-8"))
        await response.write(b"\r\n")
        await response.write(data)
        await response.write(b"\r\n")
    return response


async def config(request):
    return web.Response(
        content_type="application/json", text=pc_factory.get_ice_config()
    )


async def on_shutdown(app):
    # close peer connections
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)


def checkDeviceReadiness():
    # print debug
    os.system("v4l2-ctl --list-devices")

    if not os.path.exists("/dev/video0") and platform.system() == "Linux":
        print("Video device is not ready")
        # print('Trying to load bcm2835-v4l2 driver...')
        # os.system('bash -c "modprobe bcm2835-v4l2"')
        sleep(1)
        sys.exit()
    else:
        os.system("v4l2-ctl -d /dev/video0 --list-formats")
        os.system("v4l2-ctl -d /dev/video0 --list-formats-ext")
        print("Video device is ready")


if __name__ == "__main__":
    checkDeviceReadiness()

    ROOT = os.path.dirname(__file__)
    pcs = set()
    camera_device = CameraDevice()

    flip = False
    try:
        if os.environ["rotation"] == "1":
            flip = True
    except:
        pass

    auth = []
    if "username" in os.environ and "password" in os.environ:
        print("\n#############################################################")
        print("Authorization is enabled.")
        print("Your balenaCam is password protected.")
        print("#############################################################\n")
        # auth.append(BasicAuthMiddleware(username = os.environ['username'], password = os.environ['password']))
    else:
        print("\n#############################################################")
        print("Authorization is disabled.")
        print("Anyone can access your balenaCam, using the device's URL!")
        print(
            "Set the username and password environment variables \nto enable authorization."
        )
        print(
            "For more info visit: \nhttps://github.com/balena-io-playground/balena-cam"
        )
        print("#############################################################\n")

    # Factory to create peerConnections depending on the iceServers set by user
    pc_factory = PeerConnectionFactory()

    app = web.Application(middlewares=auth)
    app.on_shutdown.append(on_shutdown)
    app.router.add_get("/", index)
    app.router.add_get("/favicon.png", favicon)
    app.router.add_get("/balena-logo.svg", balena_logo)
    app.router.add_get("/balena-cam.svg", balena)
    app.router.add_get("/client.js", javascript)
    app.router.add_get("/style.css", stylesheet)
    app.router.add_post("/offer", offer)
    app.router.add_get("/mjpeg", mjpeg_handler)
    app.router.add_get("/ice-config", config)
    web.run_app(app, port=80)
